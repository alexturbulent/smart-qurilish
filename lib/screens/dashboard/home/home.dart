import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'home_content.dart';
import '../projects/projects.dart';
import '../settings/settings.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int projectCount = 2;
  int finishedProjects = 0;
  String totalSum = '120 000 200';
  String totalSumDollar = '250 000';
  int _selectedIndex = 0;

  static List<Widget> _widgetOptions = <Widget>[
    HomeContent(),
    Projects(),
    Settings(),
  ];

  @override
  Widget build(BuildContext context) {
    void _onItemTapped(int index) {
      setState(() {
        _selectedIndex = index;
      });
    }

    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Container(
          width: 120,
          child: SvgPicture.asset('assets/logo.svg'),
        ),
        backgroundColor: Colors.white,
        actionsIconTheme: IconThemeData(
          color: Colors.grey,
          size: 30,
        ),
        actions: <Widget>[
          Row(
            children: <Widget>[
              _selectedIndex == 1 ? Padding(
                padding: EdgeInsets.only(right: 10),
                child: Icon(
                  Icons.filter_list,
                ),
              ) : SizedBox(),
              Padding(
                padding: const EdgeInsets.only(right: 10),
                child: Icon(
                  Icons.notifications_none,
                ),
              ),
            ],
          )
        ],
      ),
      body: SafeArea(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.view_carousel),
            title: Text('Home'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.folder_open),
            title: Text('Projects'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            title: Text('Settings'),
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.red,
        onTap: _onItemTapped,
      ),
    );
  }
}
