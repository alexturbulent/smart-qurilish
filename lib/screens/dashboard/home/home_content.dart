import 'package:flutter/material.dart';
import '../../../widgets/project_card.dart';

class HomeContent extends StatelessWidget {
  final int projectCount = 2;
  final int finishedProjects = 0;
  final String totalSum = '120 000 200';
  final String totalSumDollar = '250 000';

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white30,
      child: ListView(
        children: _mainSections() +_projectCardList(),
      ),
    );


  }

  List<Widget> _mainSections() {
    return <Widget>[
      Container(
        margin: EdgeInsets.only(top: 15, right: 15, bottom: 7.5, left: 15),
        child: GestureDetector(
          onTap: () {
            print('yo negga');
          },
          child: Container(
            padding: EdgeInsets.all(15),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Colors.deepPurpleAccent,
              boxShadow: [
                BoxShadow(color: Colors.black12, blurRadius: 10),
              ],
            ),
            width: double.infinity,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(bottom: 10),
                      child: Text(
                        'Loyihalar: $projectCount',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                        ),
                      ),
                    ),
                    Text(
                      'Tugallangan: $finishedProjects',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                      ),
                    ),
                  ],
                ),
                Image.asset(
                  'assets/open_folder.png',
                ),
              ],
            ),
          ),
        ),
      ),
      Container(
        margin: EdgeInsets.only(top: 7.5, right: 15, bottom: 7.5, left: 15),
        child: GestureDetector(
          onTap: () {
            print('yo negga');
          },
          child: Container(
            padding: EdgeInsets.all(15),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Colors.purple,
              boxShadow: [
                BoxShadow(color: Colors.black12, blurRadius: 10),
              ],
            ),
            width: double.infinity,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(bottom: 10),
                      child: Text(
                        'Umumiy summa',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                        ),
                      ),
                    ),
                    Text(
                      '$totalSum sum',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                      ),
                    ),
                  ],
                ),
                Image.asset(
                  'assets/bank_orange.png',
                ),
              ],
            ),
          ),
        ),
      ),
      Container(
        margin: EdgeInsets.only(top: 7.5, right: 15, left: 15),
        child: GestureDetector(
          onTap: () {
            print('yo negga');
          },
          child: Container(
            padding: EdgeInsets.all(15),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.blueAccent,
                boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 10)]),
            width: double.infinity,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(bottom: 10),
                      child: Text(
                        'Umumiy summa dollarda',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                        ),
                      ),
                    ),
                    Text(
                      '$totalSumDollar \$',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                      ),
                    ),
                  ],
                ),
                Image.asset(
                  'assets/bank_green.png',
                ),
              ],
            ),
          ),
        ),
      ),
    ];
  }

  List<Widget> _projectCardList() {
    return <Widget>[
      ProjectCard(),
      ProjectCard(),
      ProjectCard(),
      ProjectCard(),
      SizedBox(
        height: 7.5,
      ),
    ];
  }
}
