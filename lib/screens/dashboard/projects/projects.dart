import 'package:flutter/material.dart';

import '../../../widgets/project_card.dart';

class Projects extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white30,
      child: ListView(
        children: <Widget>[
          SizedBox(
            height: 7.5,
          ),
        ]..addAll(_projectCardList()),
      ),
    );
  }

  List<Widget> _projectCardList() {
    return <Widget>[
      ProjectCard(),
      SizedBox(
        height: 7.5,
      ),
    ];
  }
}