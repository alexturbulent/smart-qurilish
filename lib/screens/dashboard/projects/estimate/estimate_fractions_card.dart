import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import './fraction_views/fraction_view_builder.dart';
import './fraction_views/fraction_view_auditor.dart';
import '../../../../widgets/progress.dart';
import '../../../../bl/statuses.dart';

class FractionOfEstimateCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Container(
          width: 120,
          child: SvgPicture.asset('assets/logo.svg'),
        ),
        iconTheme: IconThemeData(
          color: Colors.grey,
        ),
        actionsIconTheme: IconThemeData(
          color: Colors.grey,
          size: 30,
        ),
        backgroundColor: Colors.white,
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 10),
            child: Icon(
              Icons.notifications_none,
            ),
          )
        ],
      ),
      body: Column(
        children: <Widget>[]
          ..add(_headerWithProgress())
          ..addAll(_estimateCardList(context)),
      ),
    );
  }

  Widget _headerWithProgress() {
    return Container(
      margin: EdgeInsets.only(top: 25, bottom: 10),
      padding: const EdgeInsets.symmetric(horizontal: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            'Smeta',
            style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: 18,
            ),
          ),
          Progress(12),
        ],
      ),
    );
  }

  List<Widget> _estimateCardList(context) {
    return <Widget>[
      _estimateCard(
          '1. Issiqlik bilan ta\'minlash', Statuses().getStatus(1), context),
      _estimateCard(
          '2. Suv tarmoqlarini ta\'minlash', Statuses().getStatus(2), context),
      _estimateCard('3. Quvurlar tizimini qayta ta\'mirlash',
          Statuses().getStatus(3), context),
    ];
  }

  Widget _estimateCard(String txt, statusObj, context) {
    int roleId = 1;
    void goToFractionView() {
      switch (roleId) {
        case 1:
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => FractionViewBuilder(),
            ),
          );
          break;
        case 2:
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => FractionViewAuditor(),
            ),
          );
          break;
      }
    }

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 7.5),
      child: GestureDetector(
        onTap: goToFractionView,
        child: Container(
          padding: EdgeInsets.all(15),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(8),
            boxShadow: [
              BoxShadow(color: Colors.black12, blurRadius: 10),
            ],
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4),
                      color: statusObj.color,
                    ),
                    child: Text(
                      statusObj.label,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 11,
                      ),
                    ),
                  ),
                  Row(
                    children: <Widget>[
                      Icon(
                        Icons.location_on,
                        color: Colors.grey,
                      ),
                      Text(
                        'Buxoro viloyati',
                        style: TextStyle(
                          color: Colors.grey,
                          fontSize: 12,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                txt,
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w500,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    child: Row(
                      children: <Widget>[
                        Icon(
                          Icons.access_time,
                          color: Colors.grey,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 5),
                          child: Text(
                            '10.03.2020 - 30.12.2020',
                            style: TextStyle(
                              color: Colors.grey,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Progress(12),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
