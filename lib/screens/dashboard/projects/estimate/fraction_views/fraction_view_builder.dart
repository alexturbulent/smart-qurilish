import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:file_picker/file_picker.dart';

import '../../../../../bl/statuses.dart';
import '../../../../../widgets/flat_button.dart';

class FractionViewBuilder extends StatefulWidget {
  @override
  _FractionViewBuilderState createState() => _FractionViewBuilderState();
}

class _FractionViewBuilderState extends State<FractionViewBuilder> {
  bool isFinishConfirmed = false;
  String filePath;

  void chooseFile() async {
    var file = await FilePicker.getFile();
    print('file name: $file');
    if(file != null) {
      setState(() {
        filePath = file.toString();
      });
    }
  }

  void goToFinishFractionView() {
    if (!isFinishConfirmed) {
      setState(() {
        isFinishConfirmed = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Container(
          width: 120,
          child: SvgPicture.asset('assets/logo.svg'),
        ),
        iconTheme: IconThemeData(
          color: Colors.grey,
        ),
        actionsIconTheme: IconThemeData(
          color: Colors.grey,
          size: 30,
        ),
        backgroundColor: Colors.white,
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 10),
            child: Icon(
              Icons.notifications_none,
            ),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'Issiqlik bilan ta\'minlash',
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 18,
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Text(
                'Трубопроводы в помещениях или на открытых площадках в пределах цехов из труб легированных сталей, монтируемые из труб и готовых деталей, на условное давление не более 2,5 мпа. диаметр трубопровода наружный, мм 159.',
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.grey,
                  height: 1.3,
                ),
              ),
              SizedBox(
                height: 15,
              ),
              isFinishConfirmed ? _reportSection() : _fractionMeta(),
              SizedBox(
                height: 50,
              ),
              CustomFlatButton(
                text: 'Vazifani tugallash',
                backgroundColor: Colors.green,
                textColor: Colors.white,
                onClick: goToFinishFractionView,
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _fractionMeta() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        _textLine('Kod', 'Ц12-1-52-7'),
        _textLine('Hajmi', '2'),
        _textLine('Birlik', '100m'),
        _textLine('Summa', '29 000 000 sum'),
        Container(
          margin: EdgeInsets.only(bottom: 10),
          child: Row(
            children: <Widget>[
              Text(
                'Holat:  ',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                ),
              ),
              Statuses().getStatusContainer(2),
            ],
          ),
        ),
        _textLine('Bajarilish muddati', '02.02.2020'),
      ],
    );
  }

  Widget _reportSection() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Hisobot xabari',
          style: TextStyle(
            fontWeight: FontWeight.w500,
            fontSize: 18,
          ),
        ),
        SizedBox(
          height: 10,
        ),
        TextField(
          maxLines: 4,
          decoration: InputDecoration(
            contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            border: OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.grey,
              ),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.green, width: 1.0),
            ),
          ),
        ),
        Container(
          width: 200,
          child: CustomFlatButton(
            text: 'Fayl tanlang',
            borderColor: Colors.grey,
            textColor: Colors.grey,
            onClick: chooseFile,
            icon: Icons.note_add,
          ),
        ),
        filePath != null ? Text(filePath) : SizedBox(),
      ],
    );
  }

  Widget _textLine(String key, String value) {
    return Container(
      margin: EdgeInsets.only(bottom: 10),
      child: RichText(
        text: TextSpan(
          style: TextStyle(
            color: Colors.black,
            fontSize: 18,
          ),
          children: [
            TextSpan(
              text: key,
            ),
            TextSpan(
              text: ':  ',
            ),
            TextSpan(
              text: value,
              style: TextStyle(
                color: Colors.grey,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
