import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:file_picker/file_picker.dart';

import '../../../../../widgets/flat_button.dart';

class FractionViewAuditor extends StatefulWidget {
  @override
  _FractionViewAuditorState createState() => _FractionViewAuditorState();
}

class _FractionViewAuditorState extends State<FractionViewAuditor> {
  bool isFinishConfirmed = false;
  String filePath;

  void chooseFile() async {
    var file = await FilePicker.getFile();
    print('file name: $file');
    if(file != null) {
      setState(() {
        filePath = file.toString();
      });
    }
  }

  void goToFinishFractionView() {
    if (!isFinishConfirmed) {
      setState(() {
        isFinishConfirmed = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Container(
          width: 120,
          child: SvgPicture.asset('assets/logo.svg'),
        ),
        iconTheme: IconThemeData(
          color: Colors.grey,
        ),
        actionsIconTheme: IconThemeData(
          color: Colors.grey,
          size: 30,
        ),
        backgroundColor: Colors.white,
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 10),
            child: Icon(
              Icons.notifications_none,
            ),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _headerWithDescription(),
            SizedBox(
              height: 15,
            ),
            _reportSection(),
            SizedBox(
              height: 50,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: CustomFlatButton(
                text: 'Vazifani tugallash',
                backgroundColor: Colors.green,
                textColor: Colors.white,
                onClick: goToFinishFractionView,
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _headerWithDescription() {
    return Padding(
      padding: const EdgeInsets.all(15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Issiqlik bilan ta\'minlash',
            style: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 18,
            ),
          ),
          SizedBox(
            height: 15,
          ),
          Text(
            'Трубопроводы в помещениях или на открытых площадках в пределах цехов из труб легированных сталей, монтируемые из труб и готовых деталей, на условное давление не более 2,5 мпа. диаметр трубопровода наружный, мм 159.',
            style: TextStyle(
              fontSize: 15,
              color: Colors.grey,
              height: 1.3,
            ),
          ),
        ],
      ),
    );
  }

  Widget _reportSection() {
    return Container(
      padding: EdgeInsets.all(15),
      decoration: BoxDecoration(
        color: Color(0xFFFCFEFF),
        borderRadius: BorderRadius.circular(8),
        border: Border.all(color: Colors.black12),
        boxShadow: [
          BoxShadow(color: Colors.black12, blurRadius: 10),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Hisobot xabari',
            style: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 18,
            ),
          ),
          SizedBox(
            height: 15,
          ),
          Text(
            'Трубопроводы в помещениях или на открытых площадках в пределах цехов из труб легированных сталей, монтируемые из труб и готовых деталей, на условное давление не более 2,5 мпа. диаметр трубопровода наружный, мм 159.',
            style: TextStyle(
              fontSize: 15,
              color: Colors.grey,
              height: 1.3,
            ),
          ),

          filePath != null ? Text(filePath) : SizedBox(),
        ],
      ),
    );
  }
}
