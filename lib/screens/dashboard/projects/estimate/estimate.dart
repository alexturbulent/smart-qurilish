import 'package:flutter/material.dart';

import '../../../../widgets/progress.dart';
import './estimate_fractions_card.dart';

class EstimateProject extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[]..add(_headerWithProgress())..addAll(_estimateCardList(context)),
    );
  }

  Widget _headerWithProgress() {
    return Container(
      margin: EdgeInsets.only(top: 25, bottom: 10),
      padding: const EdgeInsets.symmetric(horizontal: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            'Smeta',
            style: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 18,
            ),
          ),
          Progress(12),
        ],
      ),
    );
  }

  List<Widget> _estimateCardList(context) {
    return <Widget>[
      _estimateCard('1. Quduqni qayta qurish', context),
      _estimateCard('2. Ichki xlorli suv tarmoqlari', context),
      _estimateCard('3. Quduqni qayta qurish', context),
    ];
  }

  Widget _estimateCard(txt, context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 7.5),
      child: GestureDetector(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => FractionOfEstimateCard(),
            ),
          );
        },
        child: Container(
          width: double.infinity,
          padding: EdgeInsets.all(15),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(8),
            boxShadow: [
              BoxShadow(color: Colors.black12, blurRadius: 10),
            ],
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                txt,
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 18,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Progress(52),
            ],
          ),
        ),
      ),
    );
  }
}
