import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import './estimate/estimate.dart';
import './about_project.dart';

class SingleProjectView extends StatefulWidget {
  @override
  _SingleProjectViewState createState() => _SingleProjectViewState();
}

class _SingleProjectViewState extends State<SingleProjectView> {
  final List<Tab> myTabs = <Tab>[
    Tab(text: 'Loyiha haqida'),
    Tab(text: 'Smeta'),
  ];

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: Container(
            width: 120,
            child: SvgPicture.asset('assets/logo.svg'),
          ),
          iconTheme: IconThemeData(
            color: Colors.grey,
          ),
          actionsIconTheme: IconThemeData(
            color: Colors.grey,
            size: 30,
          ),
          backgroundColor: Colors.white,
          actions: <Widget>[
            Padding(
              padding: const EdgeInsets.only(right: 10),
              child: Icon(
                Icons.notifications_none,
              ),
            )
          ],
          bottom: TabBar(
            labelColor: Colors.black,
            tabs: myTabs,
          ),
        ),
        body: TabBarView(
          children: <Widget>[
            AboutProject(),
            EstimateProject(),
          ],
        ),
      ),
    );
  }
}
