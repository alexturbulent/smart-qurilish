import 'package:flutter/material.dart';

class AboutProject extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          _projectCard(),
          SizedBox(
            height: 5,
          ),
          _projectMeta(),
          SizedBox(
            height: 5,
          ),
          _projectTenderMeta(),
        ],
      ),
    );
  }

  Widget _projectCard() {
    return Container(
      padding: EdgeInsets.all(15),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8),
        boxShadow: [
          BoxShadow(color: Colors.black12, blurRadius: 10),
        ],
      ),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(4),
                  color: Colors.green,
                ),
                child: Text(
                  'Yangi qurilish',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 11,
                  ),
                ),
              ),
              Row(
                children: <Widget>[
                  Icon(
                    Icons.location_on,
                    color: Colors.grey,
                  ),
                  Text(
                    'Buxoro viloyati',
                    style: TextStyle(
                      color: Colors.grey,
                      fontSize: 12,
                    ),
                  ),
                ],
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            '“Farg‘ona issiqlik manbai” AJ orqali Marg‘ilon shahridagi 130 ta ko‘p xonadonli uylarni issiqlik bilan taʼminlash',
            style: TextStyle(
              fontSize: 14,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            children: <Widget>[
              Icon(
                Icons.access_time,
                color: Colors.grey,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 5),
                child: Text(
                  '10.03.2020 - 30.12.2020',
                  style: TextStyle(
                    color: Colors.grey,
                  ),
                ),
              ),
            ],
          ),
          Row(
            children: <Widget>[
              Icon(
                Icons.attach_money,
                color: Colors.grey,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 5),
                child: Text(
                  '182 000',
                  style: TextStyle(
                    color: Colors.grey,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _projectMeta() {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.all(15),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8),
//        boxShadow: [
//          BoxShadow(color: Colors.black12, blurRadius: 10),
//        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _textLine('Loyiha turi', 'Kanalizatsiya'),
          _textLine('Aholi', '29800'),
          _textLine('Tashkilot', '20'),
          _textLine('Buyurtmachi',
              'Suv ta’minoti va oqova suv ob’yektlari qurilishi bo’yicha injiniring kompaniyasi” DUK Farg’ona viloyati hududiy boshqarmasi'),
          _textLine('Bosh loyihachi', 'Egamberdiyev Abror'),
          _textLine('Moliyashtirish manbai', 'Respublika byudjeti'),
          _textLine('Moliyalashtirish turi', '"Obod mahalla" dasturi'),
          _textLine('Texnik vazifalar fayli', 'Fayl'),
          _textLine('Quruvchi', '"Stroy buildings" kompaniyasi'),
        ],
      ),
    );
  }

  Widget _projectTenderMeta() {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.all(15),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8),
//        boxShadow: [
//          BoxShadow(color: Colors.black12, blurRadius: 10),
//        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _textLine('Tender raqami va to\'lov', 'E-12, 250 000'),
          _textLine('Tender sanasi', '02.02.20'),
          _textLine('Tender summasi', '698 000 000'),
          _textLine('To\'langan summa', '0 so\'m (0%)'),
          _textLine('Tender fayli', 'Fayl nomi'),
          _textLine('Tender tavsifi',
              '“Suv ta’minoti va oqova suv ob’yektlari qurilishi bo’yicha injiniring kompaniyasi” DUK Farg’ona viloyati hududiy boshqarmasi'),
        ],
      ),
    );
  }

  Widget _textLine(String key, String value) {
    return Container(
      margin: EdgeInsets.only(bottom: 10),
      child: RichText(
        text: TextSpan(
          style: TextStyle(
            color: Colors.black,
          ),
          children: [
            TextSpan(
              text: key,
            ),
            TextSpan(
              text: ':  ',
            ),
            TextSpan(
              text: value,
              style: TextStyle(
                color: Colors.grey,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
