import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:url_launcher/url_launcher.dart';

import '../screens/splash.dart';
import '../widgets/flat_button.dart';

class Intro extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/intro_gradient_bg.png'),
          fit: BoxFit.fill,
        ),
      ),
      child: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/intro_bg.png'),
            fit: BoxFit.cover,
          ),
        ),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            _appLogoBox(),
            _actionPart(context),
          ],
        ),
      ),
    );
  }

  Widget _appLogoBox() {
    return Container(
      width: 200,
      margin: EdgeInsets.only(top: 30),
      child: SvgPicture.asset('assets/logo.svg'),
    );
  }

  Widget _actionPart(context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Container(
                width: 150,
                child: CustomFlatButton(
                  text: 'Русский',
                  borderColor: Colors.white,
                  textColor: Colors.white,
                  onClick: () {},
                ),
              ),
              Container(
                width: 150,
                child: CustomFlatButton(
                  text: 'O\'zbekcha',
                  borderColor: Colors.green,
                  textColor: Colors.white,
                  backgroundColor: Colors.green,
                  icon: Icons.check,
                  onClick: () {},
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            child: CustomFlatButton(
              text: 'Boshlash',
              borderColor: Colors.red,
              textColor: Colors.white,
              backgroundColor: Colors.red,
              icon: Icons.keyboard_arrow_right,
              onClick: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Splash()),
                );
              },
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10, bottom: 10),
            padding: EdgeInsets.symmetric(horizontal: 25),
            child: RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                style: TextStyle(
                  color: Colors.white70,
                  fontSize: 15,
                ),
                children: [
                  TextSpan(
                    text: 'Kirish tugmasini bosganda ',
                  ),
                  TextSpan(
                    text: 'Foydalanish shartlariga ',
                    style: TextStyle(decoration: TextDecoration.underline),
                    recognizer: TapGestureRecognizer()
                      ..onTap = () {
                        print('link tapped');
                      },
                  ),
                  TextSpan(
                    text: 'rozi bo’lgan bo‘lasiz',
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  //TODO link to web view or browser
  Future<void> _launchInWebViewOrVC(String url) async {
    if (await canLaunch(url)) {
      await launch(
        url,
        forceSafariVC: true,
        forceWebView: true,
        headers: <String, String>{'my_header_key': 'my_header_value'},
      );
    } else {
      throw 'Could not launch $url';
    }
  }
}
