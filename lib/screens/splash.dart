import 'package:flutter/material.dart';
import '../screens/login.dart';
import '../widgets/flat_button.dart';

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  PageController _pageController;
  final int pagesCount = 3;
  int pageIndex = 0;

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
  }

  void _callNextPage() {
    setState(() {
      pageIndex += 1;

      if (pageIndex < pagesCount) {
        _pageController.animateToPage(
          pageIndex,
          duration: Duration(milliseconds: 200),
          curve: Curves.easeIn,
        );
      } else {
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (BuildContext context) => Login()),
          ModalRoute.withName('/'),
        );
      }
    });
  }

  void _jumpToLogin() {
    Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(builder: (BuildContext context) => Login()),
      ModalRoute.withName('/'),
    );
  }

  @override
  Widget build(BuildContext context) {
    return PageView(
      physics: NeverScrollableScrollPhysics(),
      controller: _pageController,
      children: _allPages(),
    );
  }

  List<Widget> _allPages() {
    return <Widget>[
      _page(
        imgPath: 'assets/splash_1_bg.png',
        text: 'Qurilish jarayonlarining real vaqtdagi nazorati',
      ),
      _page(
        imgPath: 'assets/splash_2_bg.png',
        text: 'Texnik nazoratchilarining onlayn hujjat qabul qilish tizimi',
      ),
      _page(
        imgPath: 'assets/splash_3_bg.png',
        text: 'Bosh nazoratchining barchaning ishini kuzatib turish imkoniyati',
      ),
    ];
  }

  Widget _page({String imgPath, String text, Function onClick}) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage(imgPath),
          fit: BoxFit.cover,
        ),
      ),
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Text(
            text,
            style: TextStyle(
              color: Colors.white,
              fontSize: 26,
              letterSpacing: 0.4,
              fontFamily: 'Open Sans',
              fontWeight: FontWeight.w600,
              decoration: TextDecoration.none,
            ),
            textAlign: TextAlign.center,
          ),
          _pageDots(),
          Padding(
            padding: const EdgeInsets.only(top: 15),
            child: CustomFlatButton(
              text: 'Keyingisi',
              backgroundColor: Colors.red,
              borderColor: Colors.red,
              textColor: Colors.white,
              onClick: _callNextPage,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 15),
            child: CustomFlatButton(
              text: 'O\'tkazib yuborish',
              textColor: Colors.white,
              onClick: _jumpToLogin,
            ),
          ),
        ],
      ),
    );
  }

  Widget _pageDots() {
    List<Widget> dots = [];

    for (int p = 0; p < pagesCount; p++) {
      dots.add(
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 5),
          child: Container(
              height: 14,
              width: 14,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: p == pageIndex ? Colors.white : Colors.white30,
              )),
        ),
      );
    }

    return Padding(
        padding: const EdgeInsets.only(top: 15),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: dots,
        ));
  }
}
