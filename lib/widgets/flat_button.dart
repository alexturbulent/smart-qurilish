import 'package:flutter/material.dart';


class CustomFlatButton extends StatelessWidget {
  static const Color transparent = Color(0x00000000);

  Color bgColor;
  Color brColor;
  Color txtColor;
  String txt;
  Function onUserClick;
  IconData btnIcon;

  CustomFlatButton({
    String text,
    Color backgroundColor,
    Color borderColor,
    Color textColor,
    Function onClick,
    IconData icon,
  }) {
    bgColor = backgroundColor;
    brColor = borderColor;
    txtColor = textColor;
    txt = text;
    onUserClick = onClick;
    btnIcon = icon;
  }

  @override
  Widget build(BuildContext context) {
//    print(bgColor);
    return FlatButton(
      color: bgColor,
      textColor: txtColor??Colors.black,
      onPressed: onUserClick,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(txt),
          _btnIcon(),
        ],
      ),
      shape: RoundedRectangleBorder(
        side: BorderSide(color: brColor??transparent, width: 1, style: BorderStyle.solid),
        borderRadius: BorderRadius.circular(8),
      ),
    );
  }

  Widget _btnIcon() {
    if (btnIcon != null) {
      return Padding(
        padding: const EdgeInsets.only(left: 10),
        child: Icon(
          btnIcon,
          size: 18,
        ),
      );
    }
    return SizedBox();
  }
}
