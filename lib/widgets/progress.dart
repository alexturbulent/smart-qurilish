import 'package:flutter/material.dart';

class Progress extends StatelessWidget {
  Progress(this.percent);

  final int percent;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 100,
      height: 20,
      child: Stack(
        overflow: Overflow.clip,
        children: <Widget>[
          Container(
            height: 20,
            width: 100,
            padding: EdgeInsets.all(3),
            decoration: BoxDecoration(
              color: Colors.black12,
              borderRadius: BorderRadius.circular(4),
            ),
          ),
          Positioned(
            child: Container(
              height: 20,
              width: percent.toDouble(),
              padding: EdgeInsets.all(3),
              decoration: BoxDecoration(
                color: Colors.greenAccent,
                borderRadius: BorderRadius.circular(4),
              ),
            ),
          ),
          Positioned(
            left: 5,
            top: 2,
            child: Text('$percent%'),
          ),
        ],
      ),
    );
  }
}
