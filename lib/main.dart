import 'package:flutter/material.dart';
import './screens/intro.dart';
import './screens/login.dart';

void main() => runApp(SmartConstruction());

class SmartConstruction extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Smart qurilish',
      theme: ThemeData(
        //TODO issue using theme colors for btn
        //#3FC685
//      //#F3596E
      ),
      home: RouteManager(),
    );
  }
}

class RouteManager extends StatelessWidget {
  //TODO where to save is it first time starting app
  final bool isFirstTime = true;

//  TODO route to login if it is not a first time
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: _queueManager(),
      ),
    );
  }

  Widget _queueManager() {
    if (isFirstTime) {
      return Container(
        child: Intro(),
      );
    } else {
      return Container(
        child: Login(),
      );
    }
  }
}