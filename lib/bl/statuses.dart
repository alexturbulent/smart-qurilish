import 'package:flutter/material.dart';

class StatusMaker {
  int id;
  String label;
  Color color;

  StatusMaker(this.id, this.label, this.color);
}

class Statuses {
  List _statuses = [
    StatusMaker(1, 'Boshlanmagan', Colors.grey),
    StatusMaker(2, 'Bajarilmoqda', Colors.orangeAccent),
    StatusMaker(3, 'Yakunlandi', Colors.lightBlue),
    StatusMaker(4, 'Tekshirildi', Colors.deepOrangeAccent),
    StatusMaker(5, 'Yopildi', Colors.green),
    StatusMaker(6, 'Qaytarildi', Colors.redAccent),
  ];

  getStatus(int id) {
    return id >= 1 ? _statuses[id - 1] : null;
  }

  getStatusContainer(int id) {
    var statusObj = _statuses[id - 1];

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(4),
        color: statusObj.color,
      ),
      child: Text(
        statusObj.label,
        style: TextStyle(
          color: Colors.white,
          fontSize: 11,
        ),
      ),
    );
  }
}
